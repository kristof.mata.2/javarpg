## Name
RPG Characters

## Description
Console based RPG simulation written in Java. Characters have attributes and gear and they can equip new items and gain levels to increase their stats. Characters can fight eachother. The user can interact with the application via a text based choose a number menu.
This application is a practice tool for Java OOP and other practices I have learned on Noroff's Java Fullstack course.

## Application
![image.png](./image.png)
![image-1.png](./image-1.png)

## Installation
Install JDK 17+
Install Intellij
Clone repository

## Usage
Run the application
Use the menu shown on screen.

## Maintainers
@kristof.mata.2

## Project status
Development has slowed down or stopped completely.
