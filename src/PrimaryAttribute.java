public class PrimaryAttribute {
    private int strenght;
    private int dexterity;
    private int intellect;
    //Constructor
    public PrimaryAttribute(int strenght, int dexterity, int intellect) {
        this.strenght = strenght;
        this.dexterity = dexterity;
        this.intellect = intellect;
    }

    public void increaseStats(int strenght, int dexterity, int intellect){
        this.strenght=this.strenght+strenght;
        this.dexterity=this.dexterity+dexterity;
        this.intellect=this.intellect+intellect;
    }

    public int getStr() {
        return strenght;
    }

    public int getDex() {
        return dexterity;
    }

    public int getInt() {
        return intellect;
    }

    @Override
    public boolean equals(Object o){
        if(this.strenght==((PrimaryAttribute)o).getStr() && this.dexterity==((PrimaryAttribute)o).getDex() && this.intellect==((PrimaryAttribute)o).getInt() ){
            return true;
        }
        else{
            return false;
        }
    }

    public String toString(){
        return "str: "+strenght+" dex: "+dexterity+" int: "+intellect;
    }
}
