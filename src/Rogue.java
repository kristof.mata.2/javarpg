import java.util.ArrayList;

public class Rogue extends Character {
    //Usable items for Ranger
    static ArrayList<Enum> gearOptions = new ArrayList<>(){
        {
            add(weaponType.Sword);
            add(weaponType.Dagger);
            add(armorType.Mail);
            add(armorType.Leather);
        }
    };
    //Constructor
    public Rogue(String name) {
        super(name, 2, 6, 1);
    }

    @Override
    public void greet() {
        System.out.println("Rogue nods at you!");
    }

    @Override
    public void levelUp() {
        this.setLevel(this.getLevel()+1);
        this.getBaseAttributes().increaseStats(1,4,1);
        System.out.println(this.getName()+" just leveled up! New level: "+this.getLevel());
    }

    @Override
    public double getDPS() {
        return this.getWeaponDPS()*(1+((double)getTotalAttributes().getDex()/100));
    }

    @Override
    public boolean equipItem(Item item) throws InvalidWeaponException,InvalidArmorException {
        return gearCheck(item, gearOptions);
    }


}
