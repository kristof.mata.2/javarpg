public class Armor extends Item{
    private armorType type;
    private int bonusStr;
    private int bonusDex;
    private int bonusInt;

    //Constructor
    public Armor(String name, int minLevel, gearSlot slot, armorType type, int bonusStr, int bonusDex, int bonusInt) {
        super(name, minLevel, slot , gearType.Armor);
        this.type = type;
        this.bonusStr = bonusStr;
        this.bonusDex = bonusDex;
        this.bonusInt = bonusInt;
    }
    //Getters
    public armorType getType() {
        return type;
    }

    public int getBonusStr() {
        return bonusStr;
    }

    public int getBonusDex() {
        return bonusDex;
    }

    public int getBonusInt() {
        return bonusInt;
    }

    public String getArmorStats(){
        return "str:"+this.bonusStr+" dex:"+this.bonusDex+" int:"+this.bonusInt;
    }

    @Override
    public String getInfo() {
        return "["+getName()+"]-(str:"+getBonusStr()+", dex:"+getBonusDex()+", "+getBonusInt()+")";
    }
}
