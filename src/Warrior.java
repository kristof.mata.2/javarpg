import java.util.ArrayList;

public class Warrior extends Character {
    //Usable items for Ranger
    static ArrayList<Enum> gearOptions = new ArrayList<>(){
        {
            add(weaponType.Axe);
            add(weaponType.Sword);
            add(weaponType.Hammer);
            add(armorType.Mail);
            add(armorType.Plate);
        }
    };
    //Constructor
    public Warrior(String name) {
        super(name, 5, 2, 1);
    }

    @Override
    public void greet() {
        System.out.println("Warrior greets you!");
    }

    @Override
    public void levelUp() {
        this.setLevel(this.getLevel()+1);
        this.getBaseAttributes().increaseStats(3,2,1);
        System.out.println(this.getName()+" just leveled up! New level: "+this.getLevel());
    }

    @Override
    public double getDPS() {
        return this.getWeaponDPS()*(1+((double)getTotalAttributes().getStr()/100));
    }

    @Override
    public boolean equipItem(Item item) throws InvalidWeaponException,InvalidArmorException{
        return gearCheck(item, gearOptions);
    }


}
