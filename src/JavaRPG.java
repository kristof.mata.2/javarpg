import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class JavaRPG {

    static ArrayList<Item> items = new ArrayList<>();//Contains the items available.
    static HashMap<String,Integer> itemsMap = new HashMap<>();//Name-Key pairs to access items more easily.
    static ArrayList<Character> heroes = new ArrayList<>();//Contains the heroes.
    static HashMap<String,Integer> heroesMap = new HashMap<>();//Name-Key pairs to access items more easily.
    static int itemCount=0;
    static int heroCount=0;

    public static void main(String[] args) {

        //Setting up new game, adding default items.
        System.out.println("New Game");
        newWeapon("Brown Staff",1,weaponType.Staff,5,1);
        newArmor("Brown Hood",1, gearSlot.Head, armorType.Cloth,0,0,5);
        newArmor("Brown Robe",1, gearSlot.Body, armorType.Cloth,0,0,5);
        newArmor("Brown Pants",1, gearSlot.Legs, armorType.Cloth,0,0,5);
        newWeapon("Rusty Sword",1,weaponType.Sword,10,1);

        //Menu logic starts here.
        boolean gameOver=false;
        boolean interactive=true;
        String gameState="0";
        Scanner input = new Scanner(System.in);
        if(interactive){
            while(!gameOver){
                switch (gameState){
                    case "0":
                        System.out.println("Choose-(1-new hero , 2-hero fight , 3-equip hero, 4-hero info, 5-gear info, 6-new gear, 7-exit)");
                        gameState = input.nextLine();
                        break;
                    case "1":
                        System.out.println("new hero's name:");
                        String newHeroName = input.nextLine();
                        System.out.println("new hero's class (mage, rogue, ranger, warrior):");
                        String newHeroClass = input.nextLine();
                        switch (newHeroClass) {
                            case "mage" -> newHero(newHeroName, classes.Mage);
                            case "rouge" -> newHero(newHeroName, classes.Rogue);
                            case "ranger" -> newHero(newHeroName, classes.Ranger);
                            case "warrior" -> newHero(newHeroName, classes.Warrior);
                            default -> System.out.println("Thats not a valid class.");
                        }
                        gameState="0";
                        break;
                    case "2":
                        System.out.println("Hero fight---------");
                        System.out.println("Heroes:");
                        for(Character i: heroes){
                            System.out.println(i.Intro());
                        }
                        System.out.println("first hero's Name:");
                        String firstHero = input.nextLine();
                        if(heroesMap.containsKey(firstHero)==false){
                            System.out.println("Hero not found.");
                            break;
                        }
                        System.out.println("second hero's Name:");
                        String secondHero = input.nextLine();
                        if(heroesMap.containsKey(secondHero)==false){
                            System.out.println("Hero not found.");
                            break;
                        }
                        if(heroes.get(heroesMap.get(firstHero)).getDPS()>heroes.get(heroesMap.get(secondHero)).getDPS()){
                            System.out.println(heroes.get(heroesMap.get(firstHero)).getName()+" won versus: "+heroes.get(heroesMap.get(secondHero)).getName());
                            heroes.get(heroesMap.get(firstHero)).levelUp();
                        }
                        else{
                            System.out.println(heroes.get(heroesMap.get(secondHero)).getName()+" won versus: "+heroes.get(heroesMap.get(firstHero)).getName());
                            heroes.get(heroesMap.get(secondHero)).levelUp();
                        }
                        gameState="0";
                        break;
                    case "3":
                        System.out.println("Heroes:");
                        for(Character i: heroes){
                            System.out.println(i.Intro()+"-----------");
                            System.out.println(i.getStats());
                            System.out.println(i.getGear());
                        }
                        System.out.println("Gear:-----------");
                        for(Item i: items){
                            System.out.println(i.getInfo());
                        }
                        System.out.println("hero's name for equip:");
                        String heroNameForEquip = input.nextLine();
                        if(heroesMap.containsKey(heroNameForEquip)==false){
                            System.out.println("Hero not found.");
                            break;
                        }
                        System.out.println("gear name for equip:");
                        String gearName = input.nextLine();
                        if(itemsMap.containsKey(gearName)==false){
                            System.out.println("Gear not found.");
                            break;
                        }
                        try{
                            heroes.get(heroesMap.get(heroNameForEquip)).equipItem(items.get(itemsMap.get(gearName)));
                        }catch (Exception e){

                        }
                        gameState="0";
                        break;
                    case "4":
                        System.out.println("Hero Info:");
                        for(Character i: heroes){
                            System.out.println(i.Intro()+"-----------");
                            System.out.println(i.getStats());
                            System.out.println(i.getGear());
                        }
                        gameState="0";
                        break;
                    case "5":
                        System.out.println("Gear Info:");
                        for(Item i: items){
                            System.out.println(i.getInfo());
                        }
                        gameState="0";
                        break;
                    case "6":
                        System.out.println("new gear's name:");
                        String newGearName = input.nextLine();
                        System.out.println("new gear's minimum level:");
                        int newGearMinLvl = Integer.parseInt(input.nextLine());
                        System.out.println("new gear's type (weapon, armor):");
                        String newGearType = input.nextLine();
                        String newWeaponType="";
                        String newArmorType="";
                        String newArmorSlot="";

                        int newWeaponDamage=0;
                        int newWeaponAPS=0;
                        int newBonusStr=0;
                        int newBonusDex=0;
                        int newBonusInt=0;


                        if(newGearType.equals("weapon")){
                            System.out.println("new weapon's type (axe, sword, wand, staff, bow, hammer, dagger):");
                            newWeaponType = input.nextLine();
                            System.out.println("new weapon's damage:");
                            newWeaponDamage = Integer.parseInt(input.nextLine());
                            System.out.println("new weapon's aps:");
                            newWeaponAPS = Integer.parseInt(input.nextLine());
                        }
                        else if(newGearType.equals("armor")){
                            System.out.println("new armor's type (cloth, leather, mail, plate):");
                            newArmorType = input.nextLine();
                            System.out.println("new armor's slot (head, body, legs:");
                            newArmorSlot = input.nextLine();
                            System.out.println("new armors' bonus strength:");
                            newBonusStr = Integer.parseInt(input.nextLine());
                            System.out.println("new armors' bonus dexterity:");
                            newBonusDex = Integer.parseInt(input.nextLine());
                            System.out.println("new armors' bonus intellect:");
                            newBonusInt = Integer.parseInt(input.nextLine());
                        }

                        switch (newGearType){
                            case "weapon":
                                switch (newWeaponType) {
                                    case "axe" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Axe, newWeaponDamage, newWeaponAPS);
                                    case "sword" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Sword, newWeaponDamage, newWeaponAPS);
                                    case "wand" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Wand, newWeaponDamage, newWeaponAPS);
                                    case "staff" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Staff, newWeaponDamage, newWeaponAPS);
                                    case "bow" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Bow, newWeaponDamage, newWeaponAPS);
                                    case "hammer" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Hammer, newWeaponDamage, newWeaponAPS);
                                    case "dagger" ->
                                            newWeapon(newGearName, newGearMinLvl, weaponType.Dagger, newWeaponDamage, newWeaponAPS);
                                    default -> System.out.println("Not a valid weapon type.");
                                }
                                break;
                            case "armor":
                                switch (newArmorType){
                                    case "cloth":
                                        switch (newArmorSlot) {
                                            case "head" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Head, armorType.Cloth, newBonusStr, newBonusDex, newBonusInt);
                                            case "body" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Body, armorType.Cloth, newBonusStr, newBonusDex, newBonusInt);
                                            case "legs" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Legs, armorType.Cloth, newBonusStr, newBonusDex, newBonusInt);
                                            default -> System.out.println("Not a valid armor slot.");
                                        }
                                        break;
                                    case "leather":
                                        switch (newArmorSlot) {
                                            case "head" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Head, armorType.Leather, newBonusStr, newBonusDex, newBonusInt);
                                            case "body" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Body, armorType.Leather, newBonusStr, newBonusDex, newBonusInt);
                                            case "legs" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Legs, armorType.Leather, newBonusStr, newBonusDex, newBonusInt);
                                            default -> System.out.println("Not a valid armor slot.");
                                        }
                                        break;
                                    case "mail":
                                        switch (newArmorSlot) {
                                            case "head" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Head, armorType.Mail, newBonusStr, newBonusDex, newBonusInt);
                                            case "body" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Body, armorType.Mail, newBonusStr, newBonusDex, newBonusInt);
                                            case "legs" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Legs, armorType.Mail, newBonusStr, newBonusDex, newBonusInt);
                                            default -> System.out.println("Not a valid armor slot.");
                                        }
                                        break;
                                    case "plate":
                                        switch (newArmorSlot) {
                                            case "head" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Head, armorType.Plate, newBonusStr, newBonusDex, newBonusInt);
                                            case "body" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Body, armorType.Plate, newBonusStr, newBonusDex, newBonusInt);
                                            case "legs" ->
                                                    newArmor(newGearName, newGearMinLvl, gearSlot.Legs, armorType.Plate, newBonusStr, newBonusDex, newBonusInt);
                                            default -> System.out.println("Not a valid armor slot.");
                                        }
                                        break;
                                    default:
                                        System.out.println("Not a valid armor type.");
                                        break;
                                }

                                break;
                            default:
                                System.out.println("Thats not a valid class.");
                        }
                        gameState="0";
                        break;
                    case "7":
                        gameOver=true;
                    default:

                }
            }
        }



    }

    //Method to add a new weapon to the game.
    public static void newWeapon(String name,int minLevel, weaponType wt, int damage , double aps){
        items.add(new Weapon(name, minLevel, wt, damage, aps));
        itemsMap.put(name,itemCount);
        System.out.println("New weapon has been added to the game: " + items.get(itemCount).getName());
        itemCount++;
    }
    //Method to add a new armor to the game.
    public static void newArmor(String name,int minLevel, gearSlot gs, armorType at, int strStat, int dexStat, int intStat){
        items.add(new Armor(name, minLevel, gs, at, strStat, dexStat, intStat));
        itemsMap.put(name,itemCount);
        System.out.println("New armor has been added to the game: " + items.get(itemCount).getName());
        itemCount++;
    }
    //Method to add a new hero to the game.
    public static void newHero(String name, classes c){
        switch (c) {
            case Mage -> {
                heroes.add(new Mage(name));
                System.out.println("New mage has entered the game: " + heroes.get(heroCount).getName());
            }
            case Rogue -> {
                heroes.add(new Rogue(name));
                System.out.println("New rogue has entered the game: " + heroes.get(heroCount).getName());
            }
            case Ranger -> {
                heroes.add(new Ranger(name));
                System.out.println("New ranger has entered the game: " + heroes.get(heroCount).getName());
            }
            case Warrior -> {
                heroes.add(new Warrior(name));
                System.out.println("New warrior has entered the game: " + heroes.get(heroCount).getName());
            }
        }
        heroes.get(heroCount).greet();
        heroesMap.put(name,heroCount);
        heroCount++;
    }
}
