import java.util.ArrayList;

public class Ranger extends Character {
    //Usable items for Ranger
    static ArrayList<Enum> gearOptions = new ArrayList<>(){
        {
            add(weaponType.Bow);
            add(armorType.Mail);
            add(armorType.Leather);
        }
    };
    //Constructor
    public Ranger(String name) {
        super(name, 1, 7, 1);
    }

    @Override
    public void greet() {
        System.out.println("Says hi to you!");
    }

    @Override
    public void levelUp() {
        this.setLevel(this.getLevel()+1);
        this.getBaseAttributes().increaseStats(1,5,1);
        System.out.println(this.getName()+" just leveled up! New level: "+this.getLevel());
    }

    @Override
    public double getDPS() {
        return this.getWeaponDPS()*(1+((double)getTotalAttributes().getDex()/100));
    }

    @Override
    public boolean equipItem(Item item) throws InvalidWeaponException,InvalidArmorException{
        return gearCheck(item, gearOptions);
    }


}
