import java.util.ArrayList;

public class Mage extends Character {
    //Usable items for Ranger
    static ArrayList<Enum> mageOptions = new ArrayList<>(){
        {
            add(weaponType.Staff);
            add(weaponType.Wand);
            add(armorType.Cloth);
        }
    };
    //Constructor
    public Mage(String name) {
        super(name, 1, 1, 8);
    }

    @Override
    public void greet() {
        System.out.println("Mage welcomes you!");
    }

    @Override
    public void levelUp() {
        this.setLevel(this.getLevel()+1);
        this.getBaseAttributes().increaseStats(1,1,5);
        System.out.println(this.getName()+" just leveled up! New level: "+this.getLevel());
    }

    @Override
    public double getDPS() {
        return this.getWeaponDPS()*(1+((double)getTotalAttributes().getInt()/100));
    }

    @Override
    public boolean equipItem(Item item) throws InvalidWeaponException,InvalidArmorException{
        return gearCheck(item, mageOptions);
    }

}
