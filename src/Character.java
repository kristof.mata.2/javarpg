
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Character {
    //Character attributes.
    private String name;
    private int level;
    //Hashmap for tracking hero's equipped items.
    private HashMap<gearSlot,Item> charGear = new HashMap<>();
    private PrimaryAttribute baseAttributes;
    //Constructor.
    public Character(String name, int strStat, int dexStat, int intStat) {
        this.name = name;
        this.level=1;
        baseAttributes = new PrimaryAttribute(strStat,dexStat,intStat);
    }
    //Abstract greet method.
    public abstract void greet();
    public abstract void levelUp();

    public abstract boolean equipItem(Item item) throws InvalidWeaponException,InvalidArmorException;
   //Return true if equipping is possible or with an Exception if not.
    public boolean gearCheck(Item item, ArrayList<Enum> usableList) throws InvalidWeaponException,InvalidArmorException{
        System.out.println(this.getName()+" tries to equip item: "+item.getName());
        if(item.getGearType()==gearType.Weapon){
            if (item.getMinLevel() <= this.getLevel() && usableList.contains(((Weapon) item).getType())) {
                charGear.put(item.getSlot(), item);
                System.out.println(" ---" + this.getName() + " equiped the item: " + item.getName());
                return true;
            } else {
                throw new InvalidWeaponException("Character level too low or invalid weapon type.");
            }
        }
        else{
            if (item.getMinLevel() <= this.getLevel() && usableList.contains(((Armor) item).getType())) {
                charGear.put(item.getSlot(), item);
                System.out.println(" ---" + this.getName() + " equiped the item: " + item.getName());
                return true;
            } else {
                throw new InvalidArmorException("Character level too low or invalid armor type.");
            }
        }

    }
    //Getters
    public abstract double getDPS();
    public String getName() {
        return name;
    }
    public Item getWeapon(){
        return charGear.get(gearSlot.Weapon);
    }
    public int getLevel() {
        return level;
    }
    public PrimaryAttribute getTotalAttributes(){
        int tempStr=0;
        int tempDex=0;
        int tempInt=0;
        if(charGear.get(gearSlot.Head)!=null){
            tempStr=tempStr+((Armor) charGear.get(gearSlot.Head)).getBonusStr();
            tempDex=tempDex+((Armor) charGear.get(gearSlot.Head)).getBonusDex();
            tempInt=tempInt+((Armor) charGear.get(gearSlot.Head)).getBonusInt();
        }
        if(charGear.get(gearSlot.Body)!=null){
            tempStr=tempStr+((Armor) charGear.get(gearSlot.Body)).getBonusStr();
            tempDex=tempDex+((Armor) charGear.get(gearSlot.Body)).getBonusDex();
            tempInt=tempInt+((Armor) charGear.get(gearSlot.Body)).getBonusInt();
        }
        if(charGear.get(gearSlot.Legs)!=null){
            tempStr=tempStr+((Armor) charGear.get(gearSlot.Legs)).getBonusStr();
            tempDex=tempDex+((Armor) charGear.get(gearSlot.Legs)).getBonusDex();
            tempInt=tempInt+((Armor) charGear.get(gearSlot.Legs)).getBonusInt();
        }

        return new PrimaryAttribute(baseAttributes.getStr() + tempStr
                                   ,baseAttributes.getDex() + tempDex
                                   ,baseAttributes.getInt() + tempInt
                                   );

    }
    public double getWeaponDPS(){
        if(this.getWeapon()!=null){
            return ((Weapon)charGear.get(gearSlot.Weapon)).getDamage()*((Weapon)charGear.get(gearSlot.Weapon)).getAps();
        }
        else{
            return 1;
        }

    }
    public String Intro(){
        return "("+this.name+" lv."+this.level+")";
    }
    public String getStats(){
        return "("+getTotalAttributes().toString()+" dps: "+getDPS()+")";
    }
    public String getGear(){
        String tempString="";
        if(charGear.get(gearSlot.Weapon)!=null){
            tempString=tempString+"Weapon: ["+charGear.get(gearSlot.Weapon).getName()+"]-("+((Weapon)charGear.get(gearSlot.Weapon)).getDamage()+"x"+((Weapon)charGear.get(gearSlot.Weapon)).getAps()+")";
        }
        if(charGear.get(gearSlot.Head)!=null){
            tempString=tempString+"\nHead: ["+charGear.get(gearSlot.Head).getName()+"]-("+((Armor)charGear.get(gearSlot.Head)).getArmorStats()+")";
        }
        if(charGear.get(gearSlot.Body)!=null){
            tempString=tempString+"\nBody: ["+charGear.get(gearSlot.Body).getName()+"]-("+((Armor)charGear.get(gearSlot.Body)).getArmorStats()+")";
        }
        if(charGear.get(gearSlot.Legs)!=null){
            tempString=tempString+"\nLegs: ["+charGear.get(gearSlot.Legs).getName()+"]-("+((Armor)charGear.get(gearSlot.Legs)).getArmorStats()+")";
        }
        if(tempString.equals("")){
            tempString=getName()+" has no gear yet.";
        }
        return tempString;
    }
    public PrimaryAttribute getBaseAttributes() {
        return baseAttributes;
    }
    //Setters
    public void setLevel(int level) {
        this.level=level;
    }
}
