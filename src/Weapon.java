public class Weapon extends Item{
    private weaponType type;
    private int damage;
    private double aps;
    //Constructor
    public Weapon(String name, int minLevel, weaponType type, int damage, double aps) {
        super(name, minLevel, gearSlot.Weapon , gearType.Weapon);
        this.type = type;
        this.damage = damage;
        this.aps = aps;
    }

    public weaponType getType() {
        return type;
    }

    public int getDamage() {
        return damage;
    }

    public double getAps() {
        return aps;
    }

    @Override
    public String getInfo() {
        return "["+getName()+"]-(damage:"+getDamage()+", aps:"+getAps()+")";
    }
}
