import java.util.HashMap;

public abstract class Item {
    private String name;
    private int minLevel;
    private gearSlot slot;
    private gearType type;
    //Constructor
    public Item(String name, int minLevel, gearSlot slot, gearType type) {
        this.name = name;
        this.minLevel = minLevel;
        this.slot = slot;
        this.type = type;
    }
    //Getters
    public int getMinLevel() {
        return minLevel;
    }
    public String getName() {
        return name;
    }
    public gearSlot getSlot() {
        return slot;
    }
    public gearType getGearType(){
        return type;
    }
    public abstract String getInfo();
}
