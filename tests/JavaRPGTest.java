import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JavaRPGTest {

    //Hero creation level is 1
    @Test
    public void mageConstructor_validInputs_shouldBeLevelOne() {
        Character test = new Mage("Test");
        int expected=1;
        int actual= test.getLevel();
        assertEquals(expected,actual);
    }

    //Heroes can level up
    @Test
    public void levelUp_validInputs_shouldBeLevelTwo() {
        Character test = new Mage("Test");
        int expected=2;
        test.levelUp();
        int actual=test.getLevel();
        assertEquals(expected,actual);
    }

    //Heroes base attributes
    @Test
    public void mageBaseAttributes_validInputs_shouldBeCorrect() {
        Character test = new Mage("Test");
        PrimaryAttribute expected=new PrimaryAttribute(1,1,8);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }
    @Test
    public void rangerBaseAttributes_validInputs_shouldBeCorrect() {
        Character test = new Ranger("Test");
        PrimaryAttribute expected=new PrimaryAttribute(1,7,1);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }
    @Test
    public void rogueBaseAttributes_validInputs_shouldBeCorrect() {
        Character test = new Rogue("Test");
        PrimaryAttribute expected=new PrimaryAttribute(2,6,1);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }
    @Test
    public void warriorBaseAttributes_validInputs_shouldBeCorrect() {
        Character test = new Warrior("Test");
        PrimaryAttribute expected=new PrimaryAttribute(5,2,1);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }

    //Heroes leveled up attributes
    @Test
    public void mageLeveledAttributes_validInputs_shouldBeCorrect() {
        Character test = new Mage("Test");
        test.levelUp();
        PrimaryAttribute expected=new PrimaryAttribute(2,2,13);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }
    @Test
    public void rangerLeveledAttributes_validInputs_shouldBeCorrect() {
        Character test = new Ranger("Test");
        test.levelUp();
        PrimaryAttribute expected=new PrimaryAttribute(2,12,2);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }
    @Test
    public void rogueLeveledAttributes_validInputs_shouldBeCorrect() {
        Character test = new Rogue("Test");
        test.levelUp();
        PrimaryAttribute expected=new PrimaryAttribute(3,10,2);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }
    @Test
    public void warriorLeveledAttributes_validInputs_shouldBeCorrect() {
        Character test = new Warrior("Test");
        test.levelUp();
        PrimaryAttribute expected=new PrimaryAttribute(8,4,2);
        PrimaryAttribute actual=test.getBaseAttributes();
        assertTrue(expected.equals(actual));
    }

    //Equipment testing
    @Test
    public void weaponHighLevel_validInputs_shouldThrowInvalidWeaponException() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",2, weaponType.Axe,1,1);
        JavaRPG.newArmor("TestArmorGood",2, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        String expected = "Character level too low or invalid weapon type.";
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestWeaponGood"))));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }
    @Test
    public void armorHighLevel_validInputs_shouldThrowInvalidArmorException() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",2, weaponType.Axe,1,1);
        JavaRPG.newArmor("TestArmorGood",2, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        String expected = "Character level too low or invalid armor type.";
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestArmorGood"))));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }
    @Test
    public void invalidWeaponType_validInputs_shouldThrowInvalidWeaponException() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,1,1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        String expected = "Character level too low or invalid weapon type.";
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestWeaponBad"))));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }
    @Test
    public void invalidArmorType_validInputs_shouldThrowInvalidArmorException() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,1,1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        String expected = "Character level too low or invalid armor type.";
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestArmorBad"))));
        String actual = exception.getMessage();
        assertEquals(expected, actual);
    }

    //Equipping successful tests
    @Test
    public void equipWeapon_validInputs_shouldReturnBoolean() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,1,1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        boolean expected = true;

        try{
            boolean actual = test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestWeaponGood")));
            assertEquals(expected, actual);
        }
        catch (Exception e){
        }
    }
    @Test
    public void equipArmor_validInputs_shouldReturnBoolean() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,1,1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        boolean expected = true;

        try{
            boolean actual = test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestArmorGood")));
            assertEquals(expected, actual);
        }
        catch (Exception e){
        }
    }

    //DPS tests
    @Test
    public void DPSnoWeaponTest_validInputs_shouldBeEqual() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,7,1.1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        double expected= 1*(1+(5/100d)); //unarmed dps * ( warrior base str multiplier *1.05)
        double actual=test.getDPS();
        assertEquals(expected, actual);
    }
    @Test
    public void DPSvalidWeaponTest_validInputs_shouldBeEqual() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,7,1.1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,0,0,5);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        try{
        test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestWeaponGood")));
        }
        catch (Exception e){
        }
        double expected= (7*1.1)*(1+(5/100d)); //axe dps * ( warrior base str multiplier *1.05)
        double actual=test.getDPS();
        assertEquals(expected, actual);
    }
    @Test
    public void DPSvalidWeaponWithArmorTest_validInputs_shouldBeEqual() {
        Character test = new Warrior("Test");
        JavaRPG.newWeapon("TestWeaponGood",1, weaponType.Axe,7,1.1);
        JavaRPG.newArmor("TestArmorGood",1, gearSlot.Body, armorType.Plate,1,0,0);
        JavaRPG.newWeapon("TestWeaponBad",1, weaponType.Bow,1,1);
        JavaRPG.newArmor("TestArmorBad",1, gearSlot.Body, armorType.Cloth,0,0,5);
        try{
            test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestWeaponGood")));
            test.equipItem(JavaRPG.items.get(JavaRPG.itemsMap.get("TestArmorGood")));
        }
        catch (Exception e){
        }
        double expected= (7*1.1)*(1+((5+1)/100d)); //axe dps * ( warrior base str multiplier *1.05) including the extra 1 point of strength from the plate armor.
        double actual=test.getDPS();
        assertEquals(expected, actual);
    }
}

